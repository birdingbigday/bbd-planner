from flask import Flask, jsonify, render_template, json, request
import requests
import os 
from EBirdAPIAdapter import EBirdAPIAdapter #imports EBird adapter class
from GoogleMapsAPIAdapter import GoogleMapsAPIAdapter
from simanneal.simanneal import Annealer


app = Flask(__name__)


@app.route("/")
def main():
	return render_template('index.html')

@app.route('/showSignUp')
def showSignUp():
	#print "show sign up"
	return render_template('signup.html')

#TODO: get rid of this route if it's okay
@app.route('/signUp', methods=["GET", "POST"])
def signUp():
	return "nothing here"

@app.route('/results', methods=["GET", "POST"])
def results():
	GoogleMaps = GoogleMapsAPIAdapter()
	EBird = EBirdAPIAdapter()

	address = request.form.get("inputBeginningAddress")
	coord = GoogleMaps.Coordinates(address)
	if(coord is None):
		return render_template('signup.html')
	lat = coord["lat"]
	lng = coord["lng"]
	hotspots = EBird.Hotspots(lat, lng)
	return render_template('results.html', address=address, lat=lat, lng=lng, hotspots=hotspots)

	

if __name__ == "__main__":
	# Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)