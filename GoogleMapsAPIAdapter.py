import requests
import json
import datetime
import time 
import sqlite3

class GoogleMapsAPIAdapter:

	def Coordinates(self, address):


		address = address.replace (" ", "+")
		resp = requests.get('https://maps.googleapis.com/maps/api/geocode/json?address={}&key=AIzaSyCMJAZ-tzn_mG7ISYNsjKVtA3epm6rgXVM'.format(address))
		if resp.status_code != 200:
			return None 
		resp = json.loads(resp.content)
		if resp["status"] != "OK":
			return None 
		# results = resp["results"][2]["geometry"]["location"]
		results = resp["results"][0]["geometry"]["location"]
		lat = results["lat"]
		lng = results["lng"]
		loc = {"lat" : lat, "lng": lng}
		return loc


	def Distance(self, origin, destination, departure_time):
		# departure time must NOT be in the past
		# t = time.strptime(departure_time, "%Y-%m-%d %H:%M") # seconds since the "Epoch" 24 hour clock
		# parse origin and destination ??
		
		conn = sqlite3.connect('birding.db')
		c = conn.cursor()
		dt = datetime.datetime.strptime(departure_time, "%Y-%m-%d %H:%M")
		t = time.mktime(dt.timetuple())
		departure_time = int(t)
		resp = requests.get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins={}&destinations={}&departure_time={}&key=AIzaSyCMJAZ-tzn_mG7ISYNsjKVtA3epm6rgXVM'.format(origin, destination, departure_time))
		if resp.status_code != 200:
			return None
		resp = json.loads(resp.content)
		if resp["status"] != "OK":
			print resp
			print "error"
			return None
		results = resp["rows"][0]["elements"][0]
		distance_text = results["distance"]["text"]
		distance_value = results["distance"]["value"]
		duration_text = results["duration"]["text"]
		duration_value = results["duration"]["value"]
		dis = {"distance_text" : distance_text, "distance_value" : distance_value, "duration_text" : duration_text, "duration_value" : duration_value}
		# hotspot to hotspot travel time by time of day 
		return dis
		"""
		origin_split = origin.split(",")
		dest_split = destination.split(",")
		origin_lat = float(origin_split[0])
		origin_lng = float(origin_split[1])
		dest_lat = float(dest_split[0])
		dest_lng = float(dest_split[1])

		hotspot1_query = "SELECT id FROM hotspots WHERE lat = " + origin_lat + " AND lng = " + origin_lng
		hsp1 = c.execute(hotspot1_query)
		hotspot_id1 = hsp1[0]

		hotspot2_query = "SELECT id FROM hotspots WHERE lat = " + dest_lat + " AND lng = " + dest_lng 
		hsp2 = c.execute(hotspot1_query)
		hotspot_id2 = hsp2[0]
		statement = "INSERT INTO hotspot_distance(hotspot_id1, hotspot_id2, time_of_day, travel_time) VALUES (" + hotspot_id1 +", " 
			+ hotspot_id2 + ", " + departure_time + ", " + duration_text + ")"
		c.execute(statement)
		"""
if __name__ == '__main__':
	obj = GoogleMapsAPIAdapter()
	coordinates = obj.Coordinates("1103 W. 30th St, Los Angeles, CA") #Example coordinates entered by user.
	print coordinates
	distance = obj.Distance('40.6655101,-73.89188969999998', '40.6655101,-73.89188969999998', '2016-10-13 18:00')
	print distance