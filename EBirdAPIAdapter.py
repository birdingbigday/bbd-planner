from hotSpotClass import HotSpot
import sqlite3
import operator

class EBirdAPIAdapter:

	#Takes lat, long coordinates and retrieves all hotspots in that county.
	#Returns list of hotspot objects.
	def Hotspots(self, lat, lng):
		conn = sqlite3.connect('birding.db')
		c = conn.cursor()

		counties_observed = {}
		hotspots = []

		#1. Probablistically find out which county the user is at. (Is there a better way?)
		lat_min = str(lat - 0.5)
		lat_max = str(lat + 0.5)
		lng_min = str(lng - 0.5)
		lng_max = str(lng + 0.5)

		query = "SELECT county_code FROM hotspots WHERE lat > " + lat_min + " AND lat < " + lat_max + " AND lng > " + lng_min + " AND lng < " + lng_max + " LIMIT 0, 30;"
		for row in c.execute(query):
			if(row[0] in counties_observed):
				counties_observed[row[0]] = counties_observed[row[0]]+1
			else:
				counties_observed[row[0]] = 0

		my_county = max(counties_observed.iteritems(), key=operator.itemgetter(1))[0] #Gets the key with the highest value pair.

		#2. List all hotspots in that county.
		query = "SELECT * FROM hotspots WHERE county_code = '"+my_county+"';"
		for row in c.execute(query):
			hotspots.append(HotSpot(row[0], row[1], row[2], row[3], row[4], row[5], row[6]))

		return hotspots


if __name__ == '__main__':
	obj = EbirdAPIAdapter()
	hotspots = obj.Hotspots(33.5, -117.1) #Example coordinates entered by user.
	for h in hotspots:
		print h.id + " " + h.name

