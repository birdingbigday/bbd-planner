from __future__ import print_function
import math
import random
from simanneal.simanneal import Annealer
from EBirdAPIAdapter import EBirdAPIAdapter


# A temporary heuristic for travel time between hotspots
def distance(latA, lngA, latB, lngB):
    a = [latA, lngA]
    b = [latB, lngB]
    """Calculates distance between two latitude-longitude coordinates."""
    R = 3963  # radius of Earth (miles)
    lat1, lon1 = math.radians(a[0]), math.radians(a[1])
    lat2, lon2 = math.radians(b[0]), math.radians(b[1])
    return math.acos(math.sin(lat1) * math.sin(lat2) +
                     math.cos(lat1) * math.cos(lat2) * math.cos(lon1 - lon2)) * R

# Runs simulated annealing, finding the best itinerary of hotspots.
class Calculator(Annealer):
    
    # pass extra data (the distance matrix) into the constructor
    def __init__(self, state, distance_matrix, n):
        self.distance_matrix = distance_matrix
        self.lat = lat
        self.lng = lng
        self.max = n
        self.n = n/2
        super(Calculator, self).__init__(state)  # important!

    def move(self):
        """Swaps two cities in the route."""
        a = random.randint(0, len(self.state) - 1)
        b = random.randint(0, len(self.state) - 1)

        #Increment or decrement cutoff number.
        coin_flip = random.randint(1, 100)
        if coin_flip > 50 and self.n < self.max:
        	self.n += 1
        elif coin_flip <= 50 and self.n > 1:
            self.n -= 1

        self.state[a], self.state[b] = self.state[b], self.state[a]

    def energy(self):
        """Finds estimate of species to be seen for a subset of this itinerary."""
        e = 0

        for i in range(self.n):
            e += self.distance_matrix[self.state[i-1].name][self.state[i].name]
        return e


if __name__ == '__main__':

    # latitude and longitude for hotspots in this county.
    EBird = EBirdAPIAdapter()

    lat = 34.023606 #Sample location: Jefferson/Hoover
    lng = -118.283881
    
    hotspots = EBird.Hotspots(lat, lng)
    begin = hotspots[0];

    # initial state, a randomly-ordered itinerary.
    init_state = hotspots
    random.shuffle(init_state)

    # create a distance matrix
    distance_matrix = {}
    for ha in hotspots:
        distance_matrix[ha.name] = {}
        for hb in hotspots:
            if hb.name == ha.name:
                distance_matrix[ha.name][hb.name] = 0.0
            else:
                distance_matrix[ha.name][hb.name] = distance(float(ha.lat), float(ha.lng), float(hb.lat), float(hb.lng))

    # Enter number of hotspots.
    init_n = len(hotspots)

    calc = Calculator(init_state, distance_matrix, init_n)
    # since our state is just a list, slice is the fastest way to copy
    calc.copy_strategy = "slice"  
    state, e = calc.anneal()

    #print result
    while state[0].name != begin.name:
        state = state[1:] + state[:1]  # rotate to start at begin
    print("%i mile route:" % e)
    for hotspot in state:
        print("\t", hotspot.name)